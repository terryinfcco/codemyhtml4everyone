# Codemy.com HTML For Everyone

## Notes from the course.

### Video 1
I'm going to put as much as possible into comments in the code, but inevitably that won't be enough.
* Need a text editor and a browser, and of course he's using windows and sublime text.

### Video 2
* HyperText Markup Language - it's not a programming language. 
* A series of tags. Most need open and closing tags but some are self closing.
* Right click on a web page and select view page source to see the html. 
* Cascading Style Sheets (CSS) styles the web site.
* Each tag is called an element and elements can have attributes.
* HTML is a nested language and usually programmers indent the nested stuff to make code more readable.

### Video 3
* Inside head tags is meta information that doesn't show up on the actual webpage.
* Inside body tags are the elements that show up on the webpage. 

### Video 4
* Search engines assign more importance to headers.
* h1 and h2 commonly used, h3 rarely, h4-h6 almost never. 

### Video 5
* You can just put text on a page without tags, but don't - use a paragraph tag. 
* Paragraphs create blocks.
* HTML ignores more than one space, and ignores crlf, so `<br>` to go to new line. 

### Video 6
* pre tag leaves text formatted as it is in your editor. Respects line feeds and extra spaces. Used for example for poetry. But not used often.
* `<pre> </pre>`

### Video 7
Blockquote and q tags have a cite attribute. You don't see it on the page, but it is useful for search engines. `<q cite="Douglas Adams">So Long And Thanks For All the Fish</q>`

### Video 8
* Links have various attributes
* target="new" opens a new tab when you click the link rather than opening the page on the same page.
* between the tags you can use images rather than text.

### Video 9
* Image tag (img) is self closing.
* src can be to internal image or one on a website
* can use attributes for height="500" width="700"
* to keep aspect ratio just use either height or width, but not both.
* alt attribute is for sight impaired or search engines.
* title attribute causes a text popup when you hover over the image.

### Video 10
* Lists are ordered or unordered
* Navbars are usually unordered lists styled with CSS
* Type attribute can be used to change numbers, but you would normally do this with CSS.
* Lists can be nested

### Video 11
* Tables have table tag, tr (table row) tags, th (table header) - optional, td (table data)
* tr attribute align "left", "right", "center" - where the data goes in the cells.
* most table attributes are deprecated and replaced by CSS styling.

### Video 12
* rowspan and colspan attributes for tables.
* Work pretty much like you'd expect

### Video 13
* CSS can be "inline" - like an HTML attribute
* Can put a style section in Head section
* External stylesheet `<link rel="stylesheet" href="css/style.css">`
* He says nearly everyone uses CSS frameworks.

### Video 14
* Bootstrap most popular CSS framework. 
* Basically link to and copy components.

### Video 15
* Can link to Bootstrap
* Usually just link to it. Link at getbootstrap.com
* He likes the starter template as a good start.
* div class="container" moves heading away from left edge.
